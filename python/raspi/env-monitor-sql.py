#!/usr/bin/python

import sys

import os

import Adafruit_DHT

import time

import smtplib

import mysql.connector

mydb = mysql.connector.connect( host="REMOVED",
    user="REMOVED",
    passwd="REMOVED",
    database="REMOVED"
)


# Parse command line parameters.
sensor_args = { '11': Adafruit_DHT.DHT11,
                '22': Adafruit_DHT.DHT22,
                '2302': Adafruit_DHT.AM2302 }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
    sensor = sensor_args[sys.argv[1]]
    pin = sys.argv[2]
else:
    print('usage: sudo ./Adafruit_DHT.py [11|22|2302] GPIOpin')
    print('example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO 4')
    sys.exit(1)

humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

            # Un-comment the line below to convert the temperature to Fahrenheit.
temperature = temperature * 9/5.0 + 32

            # Note that sometimes you won't get a reading and
            # the results will be null (because Linux can't
            # guarantee the timing of calls to read the sensor).
            # If this happens try again!

temp = '{0:01f}'.format(temperature) 
hum= '{0:0.1f}'.format(humidity)

mycursor = mydb.cursor()
sql = "INSERT INTO Entries (NodeID, Temperature, Humidity) VALUES (%s, %s, %s)"
val = ("5", temp, hum)
mycursor.execute(sql, val)
mydb.commit()
mycursor.close()
mydb.close()

