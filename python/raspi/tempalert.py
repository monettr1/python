#I wrote this quite a while back.  Needs to be updated to support python3.

import Adafruit_DHT

import time

import smtplib

sender = 'sender@address.com'
receivers = ['rec@address.com'] 

message = """From: Server Room <sender@address.com>
To: whoever <rec@address.com>
Subject: Server Room Temp

Server Room temp is in excess of 80 degrees F.  Please investigate.
"""

# Parse command line parameters.
sensor_args = { '11': Adafruit_DHT.DHT11,
                '22': Adafruit_DHT.DHT22,
                '2302': Adafruit_DHT.AM2302 }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
    sensor = sensor_args[sys.argv[1]]
    pin = sys.argv[2]
else:
    print('usage: sudo ./Adafruit_DHT.py [11|22|2302] GPIOpin#')
    print('example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO #4')
    sys.exit(1)
i = 1
while i == 1:

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

# Un-comment the line below to convert the temperature to Fahrenheit.
    temperature = temperature * 9/5.0 + 32

# Note that sometimes you won't get a reading and
# the results will be null (because Linux can't
# guarantee the timing of calls to read the sensor).
# If this happens try again!
    if humidity is not None or 50.0 and temperature is not None:
       file = open("/var/www/html/index.html", "a")
       file.write(time.strftime("%m/%d/%Y %H:%M:%S "))
       file.write('<div id=\"row\">Temp={0:0.1f}*Humidity={1:0.1f}%</div>'.format(temperature, humidity))
       #file.write('Temp={0:0.1f}*  Humidity={1:0.1f}%<br>'.format(temperature, humidity))
       file.close()
       if temperature > 80:
	   try:
              smtpObj = smtplib.SMTP('localhost')
              smtpObj.sendmail(sender, receivers, message)         
              print "Successfully sent email"
              time.sleep(300)
           except SMTPException:
              print "Error: unable to send email"
       else:
	      time.sleep(300)
    else:
       print('')

